const CryptoJS = require("crypto-js");
require("dotenv").config();
//const jwt = require("jsonwebtoken");
const User = require("../../modules/user");

const register = async (req, res) => {
    try {
        const newUser = new User({
            name: req.body.name,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            address: {
                address: req.body.address,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                zipcode: req.body.zipcode,
            },
            //password: req.body.name + "@S",
            password: await CryptoJS.AES.encrypt(
                req.body.name + "@S",
                process.env.PASS_SEC
            ).toString(),
            gender: req.body.gender,
            dob: req.body.dob,
            category: req.body.category,
        });
        const Users = await newUser.save();
        const { password, _id, __v, isAdmin, ...others } = newUser._doc;
        res.status(200).send({ ...others });
    } catch (e) {
        res.status(500).send(e.message);
    }
};

module.exports = { register };
