const Company = require("../../modules/company");

const company = async (req, res) => {
    try {
        const company = await Company.findById(req.params.id);
        if (!company) {
            return res.send(`Company not Found`);
        } else {
            const { password, _id, __v, ...others } = company._doc;
            res.status(200).send({ ...others });
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { company };
