const CryptoJS = require("crypto-js");
require("dotenv").config();
const Company = require("../../modules/company");

const companyLogin = async (req, res) => {
    try {
        const company = await Company.findOne({
            email: req.body.email,
        });
        if (!company) {
            return res.status(401).json("Company not found");
        } else {
            const hashPass = CryptoJS.AES.decrypt(
                company.password,
                process.env.PASS_SEC
            );
            const originalPassword = hashPass.toString(CryptoJS.enc.Utf8);
            const inputPassword = req.body.password;
            if (originalPassword != inputPassword) {
                return res.status(401).json("Wrong Password");
            } else {
                const { password, _id, __v, isAdmin, ...others } = company._doc;
                res.status(200).json({ ...others });
            }
        }
    } catch (error) {
        res.send(error.message);
    }
    //res.send("LoginPage");
};

module.exports = { companyLogin };
