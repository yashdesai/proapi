const express = require("express");
const app = express();
require("../db/con");
require("dotenv").config();
const userRoute = require("../routes/user");
const companyRoute = require("../routes/company");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//routes
app.use("/users", userRoute);
app.use("/company", companyRoute);

app.listen(process.env.PORT || 5000, () => {
    console.log("Server is running!");
});
