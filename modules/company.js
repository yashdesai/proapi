const mongoose = require("mongoose");

const companySchema = new mongoose.Schema(
    {
        isAdmin: { type: Boolean, default: false },
        name: {
            type: String,
            required: true,
        },
        address: {
            aria_address: { type: String },
            city: { type: String },
            state: { type: String },
            Country: { type: String, default: "India" },
            zipcode: { type: Number, default: 000000 },
        },
        email: {
            type: String,
            required: true,
            unique: true,
            loadClass: true,
        },
        phoneNo: {
            type: Number,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            required: true,
        },
        about: {
            type: String,
        },
        category: {
            type: String,
        },
        acquisitions: {
            Announced_Date: { type: String },
            Price: { type: Number },
        },
        funding_received: {
            type: Number,
        },
        emp_profile: {
            founder: { type: String },
            co_founder: { type: String },
            ceo: { type: String },
            cto: { type: String },
            vice_president: { type: String },
        },

    },
    { timestamps: true }
);

const Company = new mongoose.model("Company", companySchema);

module.exports = Company;
