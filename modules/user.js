const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
    {
        isAdmin: { type: Boolean, default: false },
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            loadClass: true,
        },
        phoneNo: {
            type: Number,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            required: true,
        },
    }
);

const User = new mongoose.model("User", userSchema);

module.exports = User;
