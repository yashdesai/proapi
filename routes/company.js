const router = require("express").Router();
const { companyLogin } = require("../controller/company/CompanyLogin");
const { companyRegister } = require("../controller/company/CompanyRegister");
const { allCompany } = require("../controller/company/allCompany");
const { company } = require("../controller/company/Company");
const { updateCompany } = require("../controller/company/updateCompany");
const { deleteCompany } = require("../controller/company/deleteCompany");

router.get("/", (req, res) => {
    res.send(`Users page`);
});

router.post("/CompanyRegister", companyRegister);
router.post("/CompanyLogin", companyLogin);
router.get("/AllCompany", allCompany);
router.get("/Company/:id", company);
router.put("/Company/:id", updateCompany);
router.delete("/Company/:id", deleteCompany);

module.exports = router;
